
### Question 1:

- What are the activities you do that make you relax - Calm quadrant?

    1. Do something tactile.
    2. Meditate—or even just consciously breathe.
    3. Do progressive muscle relaxation.
    4. Stretching & practising sports.
    5. Listening to soft music.

### Question 2:

- When do you find getting into the Stress quadrant?

1. If my code fails after hours of hardwork.
2. when my laptop breaks down, and realise i have not pushed my code to git.
3. If I am unable to doing some importent work. 

### Question 3:

- How do you understand if you are in the Excitement quadrant?

1. I having the plane to do some new thing it will comes occure then I have Excitement.
2. when others bring any kind of my faverite then I have Excitement.

### Question 4:

- Paraphrase the Sleep is your Superpower video in detail.

Sleep is important because it can help us physically heal, recover from illness, deal with stress, solve problems, consolidate memories, and improves motor skills. A good night's sleep isn't just about how many hours of sleep you get, but also the quality of that sleep.

### Question 5:

- What are some ideas that you can implement to sleep better?

1. Be consistent.
2. Make sure bedroom is quiet, dark, relaxing, and at a comfortable temperature.
3. Remove electronic devices, such as TVs, computers, and smart phones, from the bedroom.
4. Avoid large meals, caffeine, and alcohol before bedtime.
5. Get some exercise.

### Question 6:

- Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

1. Exercise boosts memory.
2. Exercise helps you focus.
3. Sweat now, sleep better later.
4. Work out and worry less.
5. Exercise your way to optimism.
6. Young at brain: The anti-aging power of exercise.

### Question 7:

- What are some steps you can take to exercise more?

1. Walk instead of drive, whenever you can.
2. Walk your children to school.
3. Take the stairs instead of the escalator or elevator.
4. Take a family walk after dinner.
5. Replace a Sunday drive with a Sunday walk.
6. Go for a half-hour walk instead of watching TV.
7. Get off the bus a stop early, and walk.
































