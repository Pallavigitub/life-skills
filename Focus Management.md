### Question 1:

- What is Deep Work?

Deep work is the ability to concentrate deeply on a difficult task for prolonged periods of time without getting distracted. It creates that intense, out-of-body kind of focus that makes you completely oblivious to what’s going on around you – the kind that produces your best work.

### Question 2:

- Paraphrase all the ideas in the above videos and this one in detail.

Optimal duration for deep work:
    When scheduling time for deep work, keep in mind that most people can't sustain more than four deep work hours per day. For example, you could schedule time for deep work between 8-10am every weekday.

Optimal duration for deep work:
    Having a clearly set date for completing a task can motivate you to get it done on time. If no deadline is set, there may not be a direct incentive for you to complete a task until a certain moment in time, which usually means that it takes longer than it should.

Summary of Deep Work Book:
    Summary. Deep work is the ability to focus without distraction on a cognitively demanding task. It's a skill that allows you to quickly master complicated information and produce better results in less time.

Deep Work Summary:

Deep Work: Professional activities performed in a state of distraction-free concentration that push your cognitive capabilities to their limit. These efforts create new value, improve your skill, and are hard to replicate.

Shallow Work: Non-cognitively demanding, logistical-style tasks, often performed while distracted. These efforts tend not to create much new value in the world and are easy to replicate.

Newport argues if you spend enough time in a state of frenetic shallowness, you permanently reduce your capacity to perform deep work.

“Deep work is not some nostalgic affectation of writers and early-twentieth-century philosophers. It’s instead a skill that has great value today.”

In order to produce the absolute best stuff you’re capable of, you need to commit to deep work.

### Question 3:

- How can you implement the principles in your day to day life?

1. List your core values, write them down and give examples of your core values in action.
2. Remind yourself daily of your core values through a vision board or positive affirmations.
3. Create a list of priority values. Not all values are equal, and some might even conflict.
4. Use your values to set goals.
5. Allow for exceptions. 
6. Strengthen your values with good habits.
7 Practice Inner Work.

### Question 4:

- Dangers of Social Media-Your key takeaways from the video

uploading inappropriate content, like embarrassing or provocative photos or videos of themselves or others. sharing personal information with strangers – for example, images, date of birth, location or address. cyberbullying. being exposed to too much targeted advertising and marketing.

10 Disadvantages of Social Networking:

- Lacks Emotional Connection.
- Gives People a License to be Hurtful.
- Decreases Face-to-Face Communication Skills.
- Conveys Inauthentic Expression of Feelings.
- Diminishes Understanding and Thoughtfulness.
- Causes Face-to-Face Interactions to Feel Disconnected.
- Facilitates Laziness.



















