
# History of NoSQL

The Relational Database Management Systems (RDBMS) model dates to the 1970s. Over the years, the model of the RDBMS has conveniently allowed developers to store data in a collection of rows, columns, and tables. Apart from information storage, RDBMS are secure, convenient, and affordable. However, the RDBMS architecture has the following limitations:

1. Schema rigidity: After defining a schema in RDBMS, you can't add new columns on the fly without affecting the entire table. Modern-day information systems require the ability to store structured, semi-structured, and unstructured data.

2. Structural limitations: The hard limits for field lengths, columns per table, and row size in traditional databases don't match the modern-day IT needs.

3. Poor interoperability: Most RDBMS applications require special integration libraries or drivers.

4. Scalability issues: The RDBMS model dates back when data was small, orderly, and neat. Modern-day massive data slows down the RDBMS applications.

5. Poor performance with distributed applications. The architecture of RDBMS performs poorly with distributed online databases.

# NoSQL:

Developers continuously work on NoSQL (not only SQL) databases to solve the above issues.
A NoSQL database provides a mechanism for storage and retrieval of data that is modeled in means other than the tabular relations used in relational databases. Such databases have existed since the late 1960s, but the name "NoSQL" was only coined in the early 21st century.

![what-is-Nosql-database1](https://cdn.educba.com/academy/wp-content/uploads/2019/05/what-is-Nosql-database1.png)

# Types of NoSQL Databases:

Four main types of NoSQL databases exist:

* Key-value databases.
* Document databases.
* Graph databases.
* column-oriented databases.

![Types of NoSQL](https://media.geeksforgeeks.org/wp-content/uploads/20220405112418/NoSQLDatabases.jpg)

## 1. Key-value Databases:

Key-value databases use an associative array model to write and read data. Associative arrays use unique identifiers (keys) to set a value. Key-value stores are also known as maps or dictionaries in different programming languages.

### The key-value databases support the following data types:

- Strings
- Lists
- Geospatial indexes
- Sets
- Hashes

The Key-value databases use an effective partitioning model that supports horizontal scaling better than any other type of database. This support for massive scalability is suitable for applications where end-users interact simultaneously with the system.

## 2. Document Databases:

Document-based databases use the JavaScript Object Notation (JSON) model to store data. The JSON structure handles big data effectively.

One advantage of document-based databases is interoperability. Developers find it easier to use the JSON data from document databases without the need for third-party libraries. This compatibility minimizes the application's development time and makes maintenance easier. Also, the JSON data format is flexible. This flexibility allows developers to evolve with the application's needs when storing unstructured data.

### Use-cases for document-based databases:

- Content Management Systems (CMS): In a blog, the document-based database represents each article as a single document. Sometimes, you may need to add a new attribute to an article. For instance, the author's location.

- Product catalogs: In an e-commerce database, each product contains different options. For example, a shirt may be available in yellow, blue and green. The same shirt can have a variant with short or long sleeves. Then different shirt options can have different prices. The representation of such information in a relational database involves multiple tables. However, a document database stores information in a single document using nested attributes.

- User Profiles: Document-based databases store personal profile information better than relational databases. This flexible schema makes a document-based database a good choice in healthcare applications where patient data varies.

## 2. Graph databases

A graph database focuses on the relationship between data elements. Each element is stored as a node (such as a person in a social media graph). The connections between elements are called links or relationships. In a graph database, connections are first-class elements of the database, stored directly. In relational databases, links are implied, using data to express the relationships.

A graph database is optimized to capture and search the connections between data elements, overcoming the overhead associated with JOINing multiple tables in SQL.

## 3. Column-oriented databases

While a relational database stores data in rows and reads data row by row, a column store is organized as a set of columns. This means that when you want to run analytics on a small number of columns, you can read those columns directly without consuming memory with the unwanted data. Columns are often of the same type and benefit from more efficient compression, making reads even faster. Columnar databases can quickly aggregate the value of a given column (adding up the total sales for the year, for example). Use cases include analytics.

# Feature if NoSQL Databases:

- Flexible Schema.
- Horizontal Scalling.
- Fast queries due to the data model.
- Easy of use for devolopers.
- Multi Model.

# Example of NoSQL

- MongoDB
- Apache CouchDB
- Oracle NoSQL Database
- Riak
- Objectivity InfiniteGraph


# Summary

NoSQL databases provide a variety of benefits including flexible data models, horizontal scaling, lightning fast queries, and ease of use for developers. NoSQL databases come in a variety of types including document databases, key-values databases, wide-column stores, and graph databases.

MongoDB is the world's most popular NoSQL database. Learn more about MongoDB Atlas, and give the free tier a try.

Excited to learn more now that you have your own Atlas account? Head over to MongoDB University where you can get free online training from MongoDB engineers and earn a MongoDB certification. The Quick Start Tutorials are another great place to begin; they will get you up and running quickly with your favorite programming language.

# References:

1. [What is noSQL](https://www.mongodb.com/nosql-explained)
2. [Types of NoSQL](https://www.geeksforgeeks.org/types-of-nosql-databases/)
3. [Features and Example of NoSQL](https://www.spiceworks.com/tech/artificial-intelligence/articles/what-is-nosql/)
4. [More info](https://www.youtube.com/watch?v=B3gJT3t8g4Q&t=2s)






























