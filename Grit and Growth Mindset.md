### Question 1

- Paraphrase (summarize) the video in a few lines. Use your own words.

Here is Grit by Angela Duck worth, which is a hope for them. The video is about zeal and hard work in achievement of goal. The author in her research on children found that the I.Q. a gold standard for prediction of intellectual excellence falls short when one works with passion and persistence.

### Question 2

- What are your key takeaways from the video to take action on?

Grit is the combination of passion (a deep, enduring knowledge of what you want) and perseverance (hard work and resilience). It's about moving in a direction with consistency and endurance, like having a clear inner compass that guides all your decisions and actions.

### Question 3

- Paraphrase (summarize) the video in a few lines in your own words.

People with a growth mindset believe their abilities and intelligence can be developed and improved through perseverance, good learning strategies and support from others. They are focused on learning rather than demonstrating their intelligence, so they pursue challenges and persevere in the face of difficulty.

### Question 4

- What are your key takeaways from the video to take action on?

1. You believe that achievements are down to effort, not just inherent talent.
2. You're willing to learn from your mistakes and find value in criticism.
3. You believe that your intelligence and ability can be developed.
4. You're willing to ask questions and admit when you don't know something.

### Question 5

- What is the Internal Locus of Control? What is the key point in the video?

Internal locus of control means that control comes from within. You have personal control over your own behavior. When you have an internal locus of control, you believe you have personal agency over your own life and actions. Because of this, these people tend to have more self-efficacy.

### Question 6

- Paraphrase (summarize) the video in a few lines in your own words.

The mindset stands in contrast to a “fixed mindset,” in which a person believes they possess a certain set of characteristics that’s unlikely to change. In the workplace, these two mindsets can appear both on an individual and at an organizational level.


### Question 7

- What are your key takeaways from the video to take action on?

1. Identify your own mindset.
2. Look at your own improvements.
3. Review the success of others.
4. Seek feedback.
5. Harness the power of 'yet'.
6. Learn something new.
7. Make mistakes.
8. Be kind to yourself.

### Question 8

- What are one or more points that you want to take action on from the manual?

I will stay with a problem till I complete it. I will not quit the problem.
I will not write a word of code that I don’t understand.


















