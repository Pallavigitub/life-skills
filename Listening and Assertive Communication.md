## Question 1 :

What are the steps/strategies to do Active Listening? (Minimum 6 points)

Steps/strategies to do Active Listening are :

1. Avoide desructed by own thought.
2. Focus on the speaker and topics said.
3. Don't start planning what to say next.
4. Listen without judging, or jumping to conclusions.
5. If imported, take a note on imported conversation.
6. Don't interrupt.
7. Paraphrase and summarise

## Question 2 :

According to Fisher's model, what are the key points of Reflective Listening? 
(Write in your own words, use simple English)

1. Listen to the speaker's message.
2. Analyze the meaning of the speaker's message.
3. Reflect the message back to the speaker.
4. Confirm that you properly understood the message.

## Question 3 :

What are the obstacles in your listening process?

1. Comparison and evaluation of what the other person is saying relative.
2. Agreeing just to stop or avoid the conversation.
3. Making assumptions or reading the mind of the speaker. 
4. Judgment of the speaker or the topic.
5. Language Barriers:It can exist when there is a language difference between the two individuals talking or when one person has a poor understanding of the spoken language.

## Question 4 :

What can you do to improve your listening?

1. To improve Communication and Interpersonal Skills.
2. Visualize what the speaker is saying.
3. Maintain eye contact with the speaker.
4. Limit judgments.
5. Wait for a pause to ask questions.
6. Empathize with the speaker.
7. Provide the speaker with feedback.
8. Keep an open mind.

## Question 5 :

When do you switch to Passive communication style in your day to day life?

For example, if we are feeling fearful that we are about to be harmed, passive communication may help to defuse the situation and aggressive communication might prevent the problem from getting worse.

## Question 6 :

When do you switch into Aggressive communication styles in your day to day life?

Rebuilding communication is even more challenging for people in recovery from addiction. They frequently feel frustrated and irritable and often act aggressively toward others.

## Question 7 :

When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Passive-aggression communication may derive temporary perceived benefits in the short-term, it can cause significant personal and/or professional damage in the long run. The strong alternative to passive-aggressiveness is to exercise incisive self-awareness, and practice highly effective communication and relational skills.

## Question 8 :

How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

Bellow points are help to make assertive communication

1. State needs and wants clearly, appropriately, and respectfully.
2. Express feelings clearly, appropriately, and respectfully.
3. Communicate respect for others.
4. Listen well without interrupting.
5. Feel in control of self.
6. Have good eye contact.
7. Speak in a calm and clear tone of voice.
8. Have a relaxed body posture.
9. Feel connected to others.
10. Stand up for their rights.






















